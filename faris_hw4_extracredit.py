#Defining the integral
print 'The integral is ' + str(-20./3.) + '.'

#For different samples
for n in [1, 5, 10, 100]:
    
    #Setting boundaries and dx size
    a = -1.0
    b = 1.0
    dx = (b-a)/n

    #Defining X points
    x_lis = []
    for i in range(n+1):
        x = a + (i*dx)
        x_lis.append(x)

    #Defining f(x)
    def f(x):
        return (x**3) + 2*(x**2) - 4

    #Midpoint Approximation
    def piecewise_linear():
        integral = 0
        for i in range(n):
            x = (x_lis[i+1]+x_lis[i])/2
            y = f(x)
            box = y*dx
            integral += box
        return integral
    
    #Trapezoid approximation
    def trapezoid():
        integral = 0
        for i in range(n):
            trap = (f(x_lis[i])+f(x_lis[i+1]))*dx/2
            integral += trap
        return integral

    #Simpson's rule
    def simpson():
        integral = ((2*piecewise_linear())+trapezoid())/3
        return integral

    #Gaussian Quadrature
    def gaussian():
        integral = 0
        return integral

    integral = -6.0 - (2.0/3.0)
    
    print ' '
    print 'For a sample of ' + str(n) + ':'
    print 'The piecewise linear method returns ' + str(piecewise_linear()) + ' with a relative error of ' + str(abs((integral-piecewise_linear())/integral)*100) + '%.'
    print 'The trapezoid method returns ' + str(trapezoid()) + ' with a relative error of ' + str(abs((integral-trapezoid())/integral)*100) + '%.'
    print 'Simpsons rule returns ' + str(simpson()) + ' with a relative error of ' + str(abs((integral-simpson())/integral)*100) + '%.'
    print 'The gaussian quadrature returns ' + str(gaussian()) + ' with a relative error of ' + str(abs((integral-gaussian())/integral)*100) + '%.'
