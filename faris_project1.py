import numpy as np
import matplotlib.pyplot as plt
from math import ceil

#Defining the size of the room and number of people in it
room_side = 20
num_people = 100
print  'The room is ' + str(room_side) + ' by ' + str(room_side) + ' big with ' + str(num_people) + ' people in it.'

#Generates a random expodential generator
def exp_random():
    x = np.random.random()
    y = -np.log(1-x)
    return y

#Defining move functions that will be used by people in the room for a single direction at a position x that move dx
def move_1d(x,dx):
    xnew = x+dx
    if xnew < 0:
        return abs(xnew)
    elif xnew > room_side:
        return abs(xnew - room_side)
    else:
        return xnew

#Defining Person class
class Person(object):
    def __init__(self, x, y):
        self.is_diseased = False
        self.is_vaccinated = False
        self.x = x
        self.y = y
    def move(self, xn, yn):
        self.x = xn
        self.y = yn
    def infect_person(self):
        self.is_diseased = True
    def vaccinate_person(self):
        self.is_vaccinated = True

#Defining sneeze function that checks if a healthy person gets infected
def sneeze(num_sick):
    prob = 0.25**num_sick
    x = np.random.random()
    if x < prob:
        return False
    else:
        return True

#Checks distance given two people
def peop_dist(p1, p2):
    dx = p1.x - p2.x
    dy = p1.y - p2.y
    return np.sqrt(dx**2 + dy**2)

#Finds people close by
def close_peop(people_dic, p, dist):
    d_list = []
    P = people_dic[p]
    for peop in people_dic:
        if peop != p:
            d = peop_dist(P, people_dic[peop])
            if d <= dist:
                d_list.append(peop)
    return d_list

#Finds people close by given position vectors
def close_peop_pos(people_dic, p, x, y, dist):
    d_list = []
    for peop in people_dic:
        if peop != p:
            d = np.sqrt((x-people_dic[peop].x)**2 + (y-people_dic[peop].y)**2)
            if d <= dist:
                d_list.append(peop)
    return d_list

#Checks if list of people are sick
def sick_list(people_dic, p_list):
    rlist = []
    for p in p_list:
        P = people_dic[p]
        if P.is_diseased == True:
            rlist.append(p)
    return rlist

#Defining people in room
def create_people(num_vac, max_dist):
    people_dic = {}
    for i in range(num_people):
        p_name = 'p' + str(i)
        x = np.random.random()*room_side
        y = np.random.random()*room_side
        while close_peop_pos(people_dic, p_name, x, y, max_dist):
            x = np.random.random()*room_side
            y = np.random.random()*room_side
        people_dic[p_name] = Person(x, y)
    #Make patient zero
    people_dic['p0'].infect_person()
    #Vaccinate people
    if num_vac > 0:
        i = 0
        while i < num_vac:
            peop = 'p'+str(i+1)
            people_dic[peop].vaccinate_person()
            i += 1
    return people_dic

#Calculates the number of people sick in simulation
def num_sick(people_dic):
    sick_num = 0
    for peop in people_dic:
        if people_dic[peop].is_diseased == True:
            sick_num += 1
    return sick_num

#Defing a function that will run a simulation
def simulation_run(num_vac, max_dist):
    history = []   #To see how people move
    people_dic = create_people(num_vac, max_dist)
    sick_num_list = [1] #To keep track of all the sick people
    time_list = [0]  #Keep track of time
    for peop in people_dic: #gets initial snapshot/conditions
        xo = people_dic[peop].x
        yo = people_dic[peop].y
        angle = np.random.random()*np.pi*2.0
        xn = move_1d(xo, np.cos(angle))
        yn = move_1d(yo, np.sin(angle))
        if close_peop_pos(people_dic, peop, xn, yn, max_dist):
            r = np.random.random()
            xn = move_1d(yo, r*np.cos(angle))
            yn = move_1d(yo, r*np.sin(angle))
        people_dic[peop].move(xn, yn)
        history.append([peop, 0, people_dic[peop].x, people_dic[peop].y, people_dic[peop].is_diseased, people_dic[peop].is_vaccinated])
    t = 0
    #Simulation setup, now it will move people and infect them until all non-vaccinated people are infected
    while num_sick(people_dic) < (num_people - num_vac):
        t += 1
        for peop in people_dic:   #Moves everyone first
            xo = people_dic[peop].x
            yo = people_dic[peop].y
            angle = np.random.random()*np.pi*2.0
            xn = move_1d(xo, np.cos(angle))
            yn = move_1d(yo, np.sin(angle))
            if close_peop_pos(people_dic, peop, xn, yn, max_dist):
                r = np.random.random()
                xn = move_1d(xo, r*np.cos(angle))
                yn = move_1d(yo, r*np.sin(angle))
            people_dic[peop].move(xn, yn)
        for peop in people_dic:    #Infects nearby people!
            if people_dic[peop].is_diseased == False and people_dic[peop].is_vaccinated == False:
                num_sick_close = len(sick_list(people_dic, close_peop(people_dic, peop, 1.0)))
                if sneeze(num_sick_close) == True:
                    people_dic[peop].infect_person()
            history.append([peop, t+1, people_dic[peop].x, people_dic[peop].y, people_dic[peop].is_diseased, people_dic[peop].is_vaccinated])
        sick_num_list.append(num_sick(people_dic))
        time_list.append(t)
    history.sort()
    print str(num_vac) + ' people are vaccinated within the simulation. Took ' + str(time_list[-1]) + ' iterations to infect everyone else.'
    return [time_list[-1], time_list, sick_num_list]

'''
history.sort()
for hist in history:
    print hist
'''
def sim_plot(num_vac, max_dist):
    simrun = simulation_run(num_vac, max_dist)
    time_list = simrun[1]
    sick_num_list = simrun[2]
    
    vac_list = []
    for i in range(len(time_list)):
        vac_list.append(num_vac)

    non_sick_list = []
    for i in range(len(time_list)):
        non_sick_list.append(num_people-sick_num_list[i])

    print 'It took ' + str(simrun[0]) + ' iterations for the simulation to run.\n'
    
    plt.plot(time_list, sick_num_list, 'go')
    plt.plot(time_list, non_sick_list, 'ko')
    plt.plot(time_list, vac_list, 'b')
    plt.xlabel('Time')
    plt.ylabel('Number of People')
    plt.title('Number of Infected, Healthy, and Vaccinated People')
    plt.show()
    
def sim_runs_final_times(n, num_vac, max_dist):
    final_times = []
    for i in range(n):
        simrun = simulation_run(num_vac, max_dist)
        final_times.append(simrun[0])
    aver = np.average(final_times)
    stddev = np.std(final_times)
    print 'The average time to infect everyone in ' + str(n) + ' simulations was ' + str(aver) + ' with standard deviations of ' + str(stddev) + '.\n'
    return aver

def final_sims(n_runs, vac_test, max_dist):
    aver_list = []
    for i in range(vac_test):
        vac_num = i * 5
        aver = sim_runs_final_times(n_runs, vac_num, max_dist)
        aver_list.append(aver)
    return aver_list

#sim_plot(10, 0.0)
#print sim_runs_final_times(10, 0)

final_aver = final_sims(1, 20, 0.25)
times = []
for i in range(20):
    times.append(i*5)
print final_aver

plt.plot(times, final_aver, 'bo')
plt.ylim(0, ceil(max(final_aver)/50)*50)
plt.xlabel('Number of vaccinated people')
plt.ylabel('Time to infected non-vaccinated people')
plt.title('Random Walk With Minimum Personal Space')
plt.savefig('/students/afaris/fall15/proj1plots/Random-MinSpace.png')
plt.show()
