import numpy as np
import matplotlib.pyplot as plt

from datetime import datetime
from datetime import timedelta

#Definging the functions
def f(x):
    return np.sin(x)
def g(x):
    return (x**3)-x-2
def y(x):
    return -6+x+(x**2)

#Defining my x range
dx = 0.1
x = np.arange(0., 5, dx)

#Finding the roots with variable including the function, max guesses, boundary conditions, and even tolerance
def roots(func, max_guesses, x_left, x_right, x_tol):
    xi = x_left
    xj = x_right
    i = 0
    if abs(func(xi)) <= x_tol:   #Checks boundary conditions
        return xi
    elif abs(func(xj)) <= x_tol:
        return xj
    elif func(xi)/func(xj) > 0:    #Checks if roots are within boundary
        x_left_new = x_left - (x_right+x_left)/2.0
        x_right_new = x_right + (x_right+x_left)/2.0
        return roots(func, max_guesses, x_left_new, x_right_new)
    while i <= max_guesses:
        i += 1
        x_test = (xj + xi)/2.
        if abs(func(x_test)) <= x_tol:
            return x_test
        elif func(x_test)/func(xi) < 0:
            xj = x_test
        elif func(x_test)/func(xj) < 0:
            xi = x_test
    return 'NA (Exceeded maximum iteration number, try different bounds)'

#Print statements
t1 = datetime.now()
print 'The root for f(x) is ' + str(roots(f, 100, 2, 4, 10**-3)) + '.'
t2 = datetime.now()
print 'The root for g(x) is ' + str(roots(g, 100, 0, 5, 10**-3)) + '.'
t3 = datetime.now()
print 'The root for y(x) is ' + str(roots(y, 100, 0, 3, 10**-3)) + '.'
t4 = datetime.now()
print ''
print 'If you want to test out the bisection algorithm, change the boundaries of f(x) to [0, 4] or the boundaries of y(x) to [0, 1.5]. This will show you that it works even if the boundary condition is a root or if the range does not contain a root initially. Also see what happens if you set the maximum number of iterations to 10 or less!'
print t2 - t1
print t3 - t2
print t4 - t3

#Plotting functions
plt.plot(x, f(x), 'b')
plt.plot(x, g(x), 'r')
plt.plot(x, y(x), 'g')
plt.plot([x[0],x[len(x)-1]], [0, 0], 'k')
plt.xlabel('x')
plt.ylabel('y')
plt.ylim(-10, 10)
plt.show()
