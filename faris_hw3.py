import numpy as np
import matplotlib.pyplot as plt

#Files to import
fl1 = '/Volumes/users/a/afaris/private/Faris_ASTR344HW/astr344_homework_materials/model_smg.dat'
fl2 = '/Volumes/users/a/afaris/private/Faris_ASTR344HW/astr344_homework_materials/ncounts_850.dat'

lum_lis=[]
lum_num_lis=[]
#Reading model_smg.dat
for line in open(fl1):
    item = line.rstrip()
    spot = item.find('\t')
    lum = np.float(item[:spot])
    lum_num = np.float(item[spot:])
    lum_lis.append(lum)
    lum_num_lis.append(lum_num)

L1 = lum_lis[1:11]
dNdL1 = []
#Converts model_smg.dat to differentials
for i in range(1,len(lum_lis)-1):
    h1 = lum_lis[i] - lum_lis[i-1]
    h2 = lum_lis[i+1] - lum_lis[i]
    dNdL = ((h1/(h2*(h1+h2)))*lum_num_lis[i+1])-(((h1-h2)/(h1*h2))*lum_num_lis[i])-((h2/(h1*(h1+h2)))*lum_num_lis[i-1])
    dNdL1.append(abs(dNdL))

L2 = []
dNdL2 = []
#Reading ncount_850.dat and converting to non-log
for line in open(fl2):
    item = line.rstrip()
    fst = np.float(item[:15])
    snd = np.float(item[15:])
    L2.append(10**fst)
    dNdL2.append(10**snd)

#Plots data
plt.plot(L2, dNdL2, 'bo')
plt.xlabel('Luminosity')
plt.xscale('log')
plt.ylabel('|dN/dL|')
plt.yscale('log')
plt.plot(L1, dNdL1, 'ro')
plt.show()
