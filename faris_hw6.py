import numpy as np
import matplotlib.pyplot as plt

from datetime import datetime
from datetime import timedelta


#Definging the functions
def f(x):
    return np.sin(x)
def g(x):
    return (x**3)-x-2
def y(x):
    return -6+x+(x**2)

#Defining my x range
dx = 0.01
x = np.arange(0., 5., dx)

#Defining a derivative function
def slope(func, pos):
    del_x = 10**-5
    y1 = func(pos)
    y2 = func(pos+del_x)
    return (y2-y1)/del_x

#Defining Planck function and constants
c = 3.*(10**10)
h = 6.626*(10**-27)
v = c/(870.*(10**-4))
k = 1.38*(10**-16)
def B(T):
    return (2*h*(v**3)/(c**2))/(np.exp(h*v/(k*T))-1)
def Bdiff(T):
    dB = B(T) - (1.25*(10**-12))
    return dB

#NR Method
def NR(func, guess, x_tol):
    xi = guess
    i = 0
    while abs(func(xi)) > x_tol:
        i += 1
        slp = slope(func,xi)
        if abs(slp) <= x_tol:
            del_x = -0.5
        else:
            del_x = func(xi)/slp
        xi -= del_x
    return xi

t1 = datetime.now()
NR(f, 1, 10**-3)
t2 = datetime.now()
NR(g, 1, 10**-3)
t3 = datetime.now()
NR(y, 1, 10**-3)
t4 = datetime.now()

print 'The time it takes to run the NR method for f(x) is ' + str(t2 - t1) + ' while it takes 0:00:00.000094 for the bisection method.'
print 'The time it takes to run the NR method for g(x) is ' + str(t3 - t2) + ' while it takes 0:00:00.000041 for the bisection method.'
print 'The time it takes to run the NR method for y(x) is ' + str(t4 - t3) + ' while it takes 0:00:00.000026 for the bisection method.'
print ''
print 'The temperature that corresponds to the intensity of 1.25e-12 at 870 microns is ' + str(NR(Bdiff, 1, 10**-22)) + ' kelvin.'
