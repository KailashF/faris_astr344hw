import numpy as np

#Sums (1/3)^n to 5
n = 0
S1 = 0
D1 = 0
while n < 5:
    xs = (np.float64(1.0/3.0))**n
    xd = (np.float32(1.0/3.0))**n
    S1 = S1 + xs
    D1 = D1 +xd
    n = n + 1

#Errors summing to 5
AbsEr1 = D1 - S1
RelEr1 = (D1 - S1)/D1
Mes1 =  'Summing (1/3)^n to n= 5 has an absolute error of '+str(AbsEr1)+' and relative error of '+str(RelEr1)
print Mes1

#Sums (1/3)^n to 20
n = 0
S2 = 0
D2 = 0
while n < 20:
    xs = (np.float64(1.0/3.0))**n
    xd = (np.float32(1.0/3.0))**n
    S2 = S2 + xs
    D2 = D2 +xd
    n = n + 1

#Errors summing to 20
AbsEr2 = D2 - S2
RelEr2 = (D2 - S2)/D2
Mes2 =  'Summing (1/3)^n to n=20 has an absolute error of '+str(AbsEr2)+' and relative error of '+str(RelEr2)
print Mes2

#Sums (4)^n to 5
n = 0
S3 = 0
D3 = 0
while n < 5:
    xs = (np.float64(4.0))**n
    xd = (np.float32(4.0))**n
    S3 = S3 + xs
    D3 = D3 +xd
    n = n + 1

#Errors summing to 5
AbsEr3 = D3 - S3
RelEr3 = (D3 - S3)/D3
Mes3 =  'Summing (4)^n to n= 5 has an absolute error of '+str(AbsEr3)+' and relative error of '+str(RelEr3)
print Mes3

#Sums (4)^n to 20
n = 0
S4 = 0
D4 = 0
while n < 20:
    xs = (np.float64(4.0))**n
    xd = (np.float32(4.0))**n
    S4 = S4 + xs
    D4 = D4 +xd
    n = n + 1

#Errors summing to 20
AbsEr4 = D4 - S4
RelEr4 = (D4 - S4)/D4
Mes4 =  'Summing (4)^n to n=20 has an absolute error of '+str(AbsEr4)+' and relative error of '+str(RelEr4)
print Mes4

print 'The calculation appears to be stable because it only involves integers and will not be simplified.'
