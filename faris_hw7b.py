import numpy as np
from datetime import datetime
from datetime import timedelta
import matplotlib.pyplot as plt

#Random number generator. Did not work as a random probability distribution. Claims there was a 90% that 10 people share the same birthday.
def LCG(turns):
    A = 154981
    C = 76
    m = 23
    lis = []
    t = datetime.now()
    seed = t.microsecond
    for i in range(turns):
        seed = (A*seed + C)%m
        lis.append(float(seed)/float(m))
    return float(seed)/float(m)

#Generates a random number from 1 - 365
def day():
    da = np.random.random()
    return int(da*364) + 1

#Takes a number of people and gives them a birthday (number)
def bday(n):
    bdays = []
    for i in range(n):
        bdays.append(day())
    return bdays

#Checks if there are similar items in the list
def list_check(lis):
    n = len(lis)
    for i in range(n):
        for j in range(i+1,n):
            if lis[i] ==lis[j]:
                return True
    return False

#Monte-Carlo's a certain amount of birthdays
def MCBD(n_bd, n_guess):
    true_num = 0.
    for i in range(n_guess):
        bdays = bday(n_bd)
        if list_check(bdays) == True:
            true_num += 1.
    return true_num/float(n_guess)

tot_peop = 10000
print 'Probabilities calculated using ' + str(tot_peop) + ' samples.'
for n in [2, 10, 20, 22, 23]:
    print 'The chances of ' + str(n) + ' people sharing the same birthday is ' + str(MCBD(n, tot_peop)*100) + '%.'
