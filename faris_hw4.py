import numpy as np
import matplotlib.pyplot as plt

#Define constants
C = 3*(10**3)
M = 0.3
L = 0.7
R = 0

#Sample number
n = 100

#Define Hubble's Constant
def H(z):
    x = (1+z)
    a = (M*(x**3))+(R*(x**2))+L
    return np.sqrt(a)

#Define trapezoid integration
def trap_int(z1, z2):
    n = 100
    dx = np.float((z2-z1)/n)
    x_ar = []
    y_ar = []
    for i in range(n):
        x = z1+(dx*i)
        y = C/H(x)
        x_ar.append(x)
        y_ar.append(y)
    integ = 0
    for i in range(n-1):
        b = x_ar[i+1]-x_ar[i]
        h = (y_ar[i+1]+y_ar[i])/2
        integ += b*h
    return integ

#Define Angular Diamter
def DA(z):
    trap = trap_int(0, z)
    da = np.float(trap/(1.0+z))
    print trap
    return da

#Plot points
def da_plot(z, n):
    z_ar = []
    da_ar = []
    for i in range(n):
        dz = z/n
        zi = i*dz
        z_ar.append(zi)
        da_ar.append(DA(zi))
    plt.plot(z_ar, da_ar)
    plt.title('Trapezoid Integration')
    plt.xlabel('Redshift')
    plt.ylabel('Angular Distance')
    plt.show()

#Plot points with np.trapz
def da_plot_test(z, n):
    z_ar = []
    y_ar = []
    da_ar = []
    for i in range(n):
        dz = z/n
        zi = dz*i
        z_ar.append(zi)
        y = C/H(zi)
        y_ar.append(y)
    for j in range(0,n):
        z = z_ar[:j+1]
        y = y_ar[:j+1]
        trap = np.trapz(y, z)
        da = np.float(trap/(1.0+z_ar[j]))
        da_ar.append(da)
        print trap, y
    plt.plot(z_ar, da_ar)
    plt.title('np.trapz Integration')
    plt.xlabel('Redshift')
    plt.ylabe('Angular Distance')
    plt.show()

#Showing plots
da_plot(5.0, 100)
da_plot_test(5.0, 100)
