import numpy as np

#Looks at a circle centered at the origin with radius 1 insides a circle with side length 2 centered at origin.
def circ_area(n):
    i = 0
    num_in = 0
    while i < n:
        x = 2. * np.random.random() - 1.
        y = 2. * np.random.random() - 1.
        r2 = (x**2) + (y**2)
        if r2 <= 1:
            num_in += 1
        i += 1
    return num_in * 4. / n

print 'For a circle with radius 1, we would expect pi to be the area.'
for i in range(1, 7):
    n = 10**i
    print 'The area of the circle with ' + str(n) + ' sample points is ' + str(circ_area(n))
